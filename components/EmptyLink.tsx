import React from "react";

type Props = {
  text: string;
};

const EmptyLink: React.FC<Props> = (props) => {
  const text2 = React.useMemo(() => {
    return props.text + "2";
  }, []);

  return <a>Without href {text2}</a>;
};
