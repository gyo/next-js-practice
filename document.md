# Next.js 環境構築メモ

2020/06/17 Next.js v9.4.4

## package.json

パッケージとインストール理由

| パッケージ                       | インストール理由   |
| -------------------------------- | ------------------ |
| next                             |                    |
| react                            | next               |
| react-dom                        | next               |
| @babel/core                      | jest               |
| @babel/preset-env                | jest               |
| @babel/preset-react              | jest, next         |
| @babel/preset-typescript         | jest, typescript   |
| @testing-library/react           |                    |
| @types/jest                      | jest               |
| @types/node                      | typescript, next   |
| @types/react                     | typescript, next   |
| @typescript-eslint/eslint-plugin | eslint, typescript |
| @typescript-eslint/parser        | eslint, typescript |
| babel-jest                       | jest               |
| eslint                           |                    |
| eslint-plugin-jsx-a11y           | eslint             |
| eslint-plugin-react              | eslint             |
| eslint-plugin-react-hooks        | eslint             |
| jest                             |                    |
| typescript                       |                    |

## babel.config.js

[Next.js で必要な設定](https://nextjs.org/docs/advanced-features/customizing-babel-config) と [Jest で必要な設定を分けて書く](https://jestjs.io/docs/ja/getting-started) 必要がある。

## jest.config.js

Next.js の [Path Aliases](https://nextjs.org/docs/advanced-features/module-path-aliases) を使っている場合は [moduleNameMapper](https://jestjs.io/docs/ja/configuration#modulenamemapper-objectstring-string--arraystring) を使って同様のエイリアスを Jest にも設定する必要がある。

## tsconfig.json

`next`, `react`, `react-dom`, `typescript`, `@types/react`, `@types/node` がインストールされている状態で、`pages/index.tsx` などを作って `npm run dev` を実行すると、Next.js が `tsconfig.json` を生成する。

Next.js を使う場合には `"jsx": "preserve"` でないといけない、などの制約があるので、生成された設定は基本的にそのまま使うほうがよい。

Next.js の [Path Aliases](https://nextjs.org/docs/advanced-features/module-path-aliases) を使う場合は `"baseUrl"` を追記する。

## .eslintrc.js .eslintignore

ESLint の設定ファイル

## Next.js, TypeScript, Jest 環境構築ログ

- [Getting Started | Next.js](https://nextjs.org/docs/getting-started)
  - `npm install next react react-dom`
  - `pages/index.tsx` を作る
  - `npm run dev`
  - Error
    - ```
      It looks like you're trying to use TypeScript but do not have the required package(s) installed.
      Please install typescript, @types/react, and @types/node by running:
      npm install --save-dev typescript @types/react @types/node
      ```
  - `npm install --save-dev typescript @types/react @types/node`
  - `npm run dev`
  - Info
    - ```
      We detected TypeScript in your project and created a tsconfig.json file for you.
      Your tsconfig.json has been populated with default values.
      ```
- [Getting Started · Jest](https://deltice.github.io/jest/docs/ja/getting-started.html) 赤いドキュメント。多分古い
  - `npm install --save-dev jest`
  - この状態では `import/export` 構文が使えないので、`babel` をインストールする
  - [Using Babel Getting Started · Jest](https://deltice.github.io/jest/docs/en/getting-started.html#using-babel)
    - `npm install --save-dev babel-jest regenerator-runtime`
      - > babel-jest is automatically installed when installing Jest and will automatically transform files if a babel configuration exists in your project. To avoid this behavior, you can explicitly reset the transform configuration option
      - > If you've turned off transpilation of ES2015 modules with the option { "modules": false }, you have to make sure to turn this on in your test enviornment.
    - `.babelrc` を作る
      - [@babel/preset-env · Babel](https://babeljs.io/docs/en/babel-preset-env#targets)
      - > Sidenote, if no targets are specified, @babel/preset-env will transform all ECMAScript 2015+ code by default.
    - この時点で、`.js` を `.ts` に書き換えてもテストは実行できる
      - ！エディタ上では型エラーが残っている
      - ！`(a: number, b: number) => {}` のような TypeScript の文法を追記すると、テストは `SyntaxError` で失敗する
  - [Using TypeScript Getting Started · Jest](https://deltice.github.io/jest/docs/en/getting-started.html#using-typescript)
    - `npm install --save-dev ts-jest @types/jest`
    - Jest のドキュメントでは次に `package.json` の `jest` セクションを追記することになっているが、それに従うとテストを実行した際に `<rootDir>/node_modules/ts-jest/preprocessor.js は ts-jest でのみ動作する` といった旨の警告が表示されたため、`ts-jest` のドキュメントを正として進めることにする
    - [kulshekhar/ts-jest: TypeScript preprocessor with sourcemap support for Jest](https://github.com/kulshekhar/ts-jest)
      - [Installing ts-jest | ts-jest](https://kulshekhar.github.io/ts-jest/user/install)
        - 必要なパッケージはすべてインストール済みのため、`npm install --save-dev jest typescript ts-jest @types/jest` は実行しない
        - `npx ts-jest config:init` を用いて、`jest.config.js` を作成する
    - この時点で、TypeScript のテストが実行できる
- [Getting Started · Jest](https://jestjs.io/docs/ja/getting-started) 緑のドキュメント。多分新しい
  - `npm install --save-dev jest`
  - この状態では `import/export` 構文が使えないので、`babel` をインストールする
  - [Using Babel Getting Started · Jest](https://jestjs.io/docs/en/getting-started#using-babel)
    - `npm i -D babel-jest @babel/core @babel/preset-env`
    - `babel.config.js` を作る
  - [Using TypeScript Getting Started · Jest](https://jestjs.io/docs/en/getting-started#using-typescript)
    - `npm i -D @babel/preset-typescript`
    - `babel.config.js` に `@babel/preset-typescript` を追記する
    - > However, there are some caveats to using TypeScript with Babel. Because TypeScript support in Babel is transpilation, Jest will not type-check your tests as they are run. If you want that, you can use ts-jest.
    - `npm i -D @types/jest`
    - この時点で、TypeScript のテストが実行できる
- React のテスト
  - [Setup without Create React App Testing React Apps · Jest](https://jestjs.io/docs/en/tutorial-react#setup-without-create-react-app)
    - `npm i -D @babel/preset-react react-test-renderer`
    - `babel.config.js` に `@babel/preset-react` を追記する
  - [react-testing-library Testing React Apps · Jest](https://jestjs.io/docs/ja/tutorial-react#react-testing-library)
    - `npm i -D @testing-library/react`
  - スナップショットテストが不要であれば `npm uninstall react-test-renderer` する
- ESLint
  - eslint
    - [Getting Started with ESLint - ESLint - Pluggable JavaScript linter](https://eslint.org/docs/user-guide/getting-started)
    - `npm install eslint --save-dev`
  - typescript に対応する
    - [The future of TypeScript on ESLint - ESLint - Pluggable JavaScript linter](https://eslint.org/blog/2019/01/future-typescript-eslint#announcing-the-typescript-eslint-project)
    - [Getting Started - Linting your TypeScript Codebase](https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/README.md)
      - `npm i --save-dev @typescript-eslint/parser @typescript-eslint/eslint-plugin`
      - `.eslintrc.js` を更新する
        - ```
          module.exports = {
            root: true,
            parser: '@typescript-eslint/parser',
            plugins: [
              '@typescript-eslint',
            ],
            extends: [
              'eslint:recommended',
              'plugin:@typescript-eslint/recommended',
            ],
          };
          ```
      - `.eslintignore` を作る
        - ```
          .next
          node_modules
          ```
      - `package.json` に script を追記する
        - [Command Line Interface - ESLint - Pluggable JavaScript linter](https://eslint.org/docs/user-guide/command-line-interface)
          - `"eslint": "eslint \"\*_/_.{ts,tsx}\""`
      - その他
        - [Extending your TypeScript linting with Type-Aware Rules](https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/README.md#extending-your-typescript-linting-with-type-aware-rules)
        - [Usage with prettier](https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/README.md#usage-with-prettier)
        - [Jest testing](https://www.npmjs.com/package/eslint-plugin-jest)
        - React best practices
          - [eslint-plugin-react](https://www.npmjs.com/package/eslint-plugin-react)
          - [eslint-plugin-react-hooks](https://www.npmjs.com/package/eslint-plugin-react-hooks)
  - eslint-plugin-react
    - [eslint-plugin-react - npm](https://www.npmjs.com/package/eslint-plugin-react)
      - `npm install eslint-plugin-react --save-dev`
      - [Recommended](https://www.npmjs.com/package/eslint-plugin-react#recommended)
      - `.eslintrc.js` の `"extends"` に `"plugin:react/recommended"` を追記する
      - 実行時に Warning が表示される
        - ```
          Warning: React version not specified in eslint-plugin-react settings. See https://github.com/yannickcr/eslint-plugin-react#configuration .
          ```
        - `.eslintrc.js` に `settings.react.version` を設定する
  - eslint-plugin-react-hooks
    - [ESLint プラグイン フックのルール – React](https://ja.reactjs.org/docs/hooks-rules.html#eslint-plugin)
    - [eslint-plugin-react-hooks - npm](https://www.npmjs.com/package/eslint-plugin-react-hooks)
      - `npm install eslint-plugin-react-hooks --save-dev`
      - `.eslintrc.js` の `"extends"` に `"plugin:react-hooks/recommended"` を追記する
      - カスタムが必要になったら `"plugins"`, `"rules"` を追記する
        - [Custom Configuration](https://www.npmjs.com/package/eslint-plugin-react-hooks#custom-configuration)
          - > If you want more fine-grained configuration, you can instead add a snippet like this to your ESLint configuration file
  - eslint-plugin-jsx-a11y
    - [eslint-plugin-jsx-a11y アクセシビリティ – React](https://ja.reactjs.org/docs/accessibility.html#eslint-plugin-jsx-a11y)
    - [jsx-eslint/eslint-plugin-jsx-a11y: Static AST checker for a11y rules on JSX elements.](https://github.com/jsx-eslint/eslint-plugin-jsx-a11y)
      - `npm install eslint-plugin-jsx-a11y --save-dev`
      - `.eslintrc.js` の `"extends"` に `"plugin:jsx-a11y/recommended"` を追記する
      - カスタムが必要になったら `"plugins"`, `"rules"` を追記する
        - [Usage](https://github.com/jsx-eslint/eslint-plugin-jsx-a11y#usage)
